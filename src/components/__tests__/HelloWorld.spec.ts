import HelloWorld from "../HelloWorld.vue";
import { shallowMount } from "@vue/test-utils";

describe("HelloWorld.vue", () => {
  test("renders prop.msg when passed", () => {
    const msg = "new message";
    const wrapper = shallowMount(HelloWorld, {
      propsData: { msg },
    });
    expect(wrapper.text()).toMatch(msg);
  });
});
